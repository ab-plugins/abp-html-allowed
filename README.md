# ABP HTML Allowed

This plugin allows some people to use HTML in some forums

## IMPORTANT
Think to **allow html in all forums**, the filtering is done on display

## SETTINGS

- **forums** : the forum(s) where you want to really allow html
- **groups** : the usergroup(s) allowed to use html
- **users** : list of comma separated (without space) users id

Note that settings are cumulatives: if you set a forum, a group and an user id, the user must be in the allowed groups and in the good forum.
