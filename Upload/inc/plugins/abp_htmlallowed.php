<?php

/**
 * Allow to limit number of posts per hour
 * Copyright 2021 CrazyCat <crazycat@c-p-f.org>
 */
if (!defined("IN_MYBB")) {
    die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}

define('CN_ABPHTML', str_replace('.php', '', basename(__FILE__)));

/**
 * Informations about the plugin
 * @global MyLanguage $lang
 * @return array
 */
function abp_htmlallowed_info() {
    global $lang;
    $lang->load(CN_ABPHTML);
    return array(
        'name' => $lang->abp_htmlallowed_name,
        'description' => $lang->abp_htmlallowed_desc . '<a href=\'https://ko-fi.com/V7V7E5W8\' target=\'_blank\'><img height=\'30\' style=\'border:0px;height:30px;float:right;\' src=\'https://az743702.vo.msecnd.net/cdn/kofi1.png?v=0\' border=\'0\' alt=\'Buy Me a Coffee at ko-fi.com\' /></a>',
        'website' => 'https://gitlab.com/ab-plugins/abp-htmlalloweder',
        'author' => 'CrazyCat',
        'authorsite' => 'http://community.mybb.com/mods.php?action=profile&uid=6880',
        'version' => '1.0',
        'compatibility' => '18*',
        'codename' => CN_ABPHTML
    );
}

/**
 * 
 * @global DB_Base $db
 * @global MyLanguage $lang
 */
function abp_htmlallowed_install() {
    global $db, $lang;
    $lang->load(CN_ABPHTML);
    $settinggroups = [
        'name' => CN_ABPHTML,
        'title' => $lang->abp_htmlallowed_name,
        'description' => $lang->abp_htmlallowed_setting_desc,
        'disporder' => 0,
        'isdefault' => 0
    ];
    $db->insert_query('settinggroups', $settinggroups);
    $gid = $db->insert_id();
    $settings = [
        [
            'gid' => $gid,
            'name' => CN_ABPHTML . '_forums',
            'title' => $lang->abp_htmlallowed_forums,
            'description' => $lang->abp_htmlallowed_forums_desc,
            'optionscode' => 'forumselect',
            'value' => '',
            'disporder' => 1
        ],
        [
            'gid' => $gid,
            'name' => CN_ABPHTML . '_groups',
            'title' => $lang->abp_htmlallowed_groups,
            'description' => $lang->abp_htmlallowed_groups_desc,
            'optionscode' => 'groupselect',
            'value' => '1,2,5,7',
            'disporder' => 2
        ],
        [
            'gid' => $gid,
            'name' => CN_ABPHTML . '_uids',
            'title' => $lang->abp_htmlallowed_uids,
            'description' => $lang->abp_htmlallowed_uids_desc,
            'optionscode' => 'text',
            'value' => '',
            'disporder' => 3
        ]
    ];
    $db->insert_query_multiple('settings', $settings);
    rebuild_settings();
}

function abp_htmlallowed_is_installed() {
    global $mybb;
    return (array_key_exists(CN_ABPHTML . '_forums', $mybb->settings));
}

function abp_htmlallowed_uninstall() {
    global $db;
    $db->delete_query('settings', "name LIKE '" . CN_ABPHTML . "%'");
    $db->delete_query('settinggroups', "name = '" . CN_ABPHTML . "'");
    rebuild_settings();
}

$plugins->add_hook('postbit', 'abp_htmlallowed_postbit');

function abp_htmlallowed_postbit(&$post) {
    global $mybb, $lang;
    if ((int) $mybb->settings[CN_ABPHTML . '_forums'] == 0 || (int) $mybb->settings[CN_ABPHTML . '_groups'] == 0) {
        $post['message'] = htmlentities($post['message']);
        return;
    }
    $can = ['forum' => 0, 'group' => 0, 'user' => 0];
    
    if ($mybb->settings[CN_ABPHTML . '_forums'] != -1) {
        $forums = explode(',', $mybb->settings[CN_ABPHTML . '_forums']);
        if (in_array($post['fid'], $forums)) {
            $can['forum'] = 1;
        }
    } else {
        $can['forum'] = 1;
    }

    if ($mybb->settings[CN_ABPHTML . '_groups'] == -1) {
        $can['group'] = 1;
    } else {
        if (!isset($post['uid']) && isset($post['edit_uid'])) {
            $puser = get_user($post['edit_uid']);
        } else {
            $puser = get_user($post['uid']);
        }
        if (is_member($mybb->settings[CN_ABPHTML . '_groups'], $puser['uid'])) {
            $can['group'] = 1;
        }
    }

    if (trim($mybb->settings[CN_ABPHTML . '_uids']) != '') {
        $users = explode(',', $mybb->settings[CN_ABPHTML . '_uids']);
        if (!isset($post['uid']) && isset($post['edit_uid'])) {
            $uid = $post['edit_uid'];
        } else {
            $uid = $post['uid'];
        }
        if (in_array($uid, $users)) {
            $can['user'] = 1;
        }
    } else {
        $can['user'] = 1;
    }
    if ($can['forum']==0 || $can['group']==0 || $can['user']==0) {
        $post['message'] = htmlentities($post['message']);
    }
    return;
}
