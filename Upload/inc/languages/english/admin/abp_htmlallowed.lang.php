<?php

$l['abp_htmlallowed_name'] = 'ABP HTML Allowed';
$l['abp_htmlallowed_desc'] = 'This plugin allows some users to use html in some forums';
$l['abp_htmlallowed_setting_desc'] = 'Settings for ABP HTML Allowed';
$l['abp_htmlallowed_forums'] = 'Forums';
$l['abp_htmlallowed_forums_desc'] = 'Choose the forum(s) where html is allowed.';
$l['abp_htmlallowed_groups'] = 'Groups';
$l['abp_htmlallowed_groups_desc'] = 'Choose the group(s) allowed to use html.';
$l['abp_htmlallowed_uids'] = 'Users ID';
$l['abp_htmlallowed_uids_desc'] = 'Users ID (comma separated).<br />If this list is empty, all members of selected groups can use html';